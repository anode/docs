Getting started in diaspora\*
=============================

Part 3 – Aspects
----------------

Think of your life and the people you know. Each person is a part of one
or more aspects of your life. They might be a member of your family, a
close friend, a work colleague or someone you play sport or music with
or with whom you share a particular interest. They might be someone you
just like sharing jokes with online. Or they might be part of more than
one of these aspects of your life.

diaspora\* works in exactly the same way. You place contacts into one or
more “aspects” of your life on diaspora\* based on which aspect(s) of
your life they are part of. In this way, you control which of your
contacts sees each post you make to diaspora\*, by posting to a
particular aspect or aspects. This means that you can easily share the
right things with the right people. We’ll explain more about what
posting to aspects means over the next three parts of this series.

#### My aspects

First, let’s look at the “My aspects” menu. From your stream, click My
aspects in the sidebar. A list of your aspects will drop down, and
you’ll see a stream containing only posts made by people you have placed
into whichever aspects are selected in the list. If you’ve only just
signed up, there will be just the four default aspects, and there might
not yet be anyone in any of your aspects. We’ll fix that very soon!

-   Right now all your aspects should be selected, with ticks next to
    them.
-   You can click aspects in the menu to select or deselect them.
-   You can select just one aspect, or any combination of them.
-   If all aspects are selected and you want to view just one, click
    Deselect all and then select the one you want.

Why not play around with it for a few seconds?

This selection system has two uses:

1.  It filters the content stream. You might want to read only posts
    written by people in your Family aspect. Or you might want to read
    everything posted by your contacts *except* family members.
2.  If you post a status message from the Aspects page, the aspects
    selected in the left-hand menu will be automatically selected in the
    publisher. For example, if the Friends, Family and Work aspects are
    selected in the list, when you click inside the publisher window
    you’ll see that the Aspects selector button reads “In 3 aspects.”

![Aspect list](images/aspect-1.png)

#### Adding an aspect

When you sign up you are given four aspects: Family, Work, Friends and
Acquaintances. Of course you won’t be able to sort all of your life into
just four standard aspects, so the next thing we’re going to do is to
add some aspects. You can also delete the default aspects if you want
to.

Note: if you’re using the mobile interface, you’ll first need to switch
to the desktop version by clicking Toggle mobile in the drawer as it is
not currently possible to add a new aspect in the mobile version.

Click My aspects in the sidebar and a list of your aspects will appear.
To add an aspect, click + Add an aspect under the list of aspects in the
menu. You will be presented with a pop-up window.

This guide uses an aspect called “Diaspora” as an example, but give your
aspect whatever name you want.

![Add aspect](images/aspect-2.png)

1.  Choose a name for the new aspect. This might reflect the common
    connection between these people, perhaps “Creative writing,”
    “Football,” or “Activists,” but you can call it whatever you like.
    **No one else will ever be able to see the names of your aspects,
    and your contacts will never know which aspect(s) they are in. That
    is your business alone.**
2.  Decide whether to check “Make contacts in this aspect visible to
    each other?” This is useful if this aspect is for a club or group in
    which all the members know each other, as making them visible to
    each other will help them to connect with each other. It might not
    be appropriate to enable this if people in the aspect don’t know
    each other.
3.  When you have finished, click the Create button. The aspect is then
    created and selected in the menu.

Once the aspect has been created, you’ll be taken to your Contacts page
and shown a list of people in the aspect you’ve just created (which will
be no one so far). Even if this aspect is empty, you will see a list of
your other contacts, with a button next to each so you can easily add
them to this new aspect.

### Contacts page

We mentioned your contacts page in [Part 2](2-interface.html).
You’re here now, but the best way to get to your contacts page from
other pages is go to your user menu from the header bar and select
Contacts from the drop-down list.

The contacts page displays a list of your aspects in the sidebar, and
the people you have placed in those aspects in the main section. (The
list of aspects appears at the top in the mobile view.)

If you are displaying contacts from all aspects (click My contacts in
the sidebar), each contact will have a green button to its right showing
which aspect(s) they have been placed in. If a contact is in more than
one aspect, the button will read “In *n* aspects.” Click the button to
see which aspects those are.

You can click this button to change the aspect(s) this person is in by
selecting and deselecting aspects in the drop-down list. If you want to
remove a person from all of your aspects, deselect all aspects and the
button will turn grey. You are no longer sharing with them.

If you are viewing contacts in an individual aspect, there will be a
cross to the right of each contact displayed. Clicking this will remove
the person from that aspect.

You can also create new aspects from your contacts page.

#### Only sharing with me

This shows you a list of people who are sharing with you but with whom
you are not sharing – your “followers.”

Now that you understand aspects, let’s make some connections.

[Part 2 – The
interface](2-interface.html) |
[Part 4 – Finding and connecting with
people](4-connecting.html)

#### Useful Resources

-   [Codebase](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Find & report bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - General](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Development](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Discussion -
    General](http://groups.google.com/group/diaspora-discuss)
-   [Discussion -
    Development](http://groups.google.com/group/diaspora-dev)

[![Creative Commons
License](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) is licensed
under a [Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/)
