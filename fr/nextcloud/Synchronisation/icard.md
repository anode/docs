# Synchronisation avec Apple Contacts (iCard)
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Allez dans les préférences système, puis cliquez sur **Comptes Internet**.

Cliquez sur **Ajouter un autre compte** puis choisissez **Ajouter un compte CardDAV**.

Dans la fenêtre **Ajouter un compte CardDAV**, renseignez votre identifiant(identifiant@framagenda.org) et votre mot de passe, et cliquez sur **Créer**.

Vos contacts devraient maintenant être ajoutés dans l'application Contacts de MacOS X.
